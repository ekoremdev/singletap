## 0.1.0

- Added executing the `onPressed` method in the `WidgetsBinding.instance.addPostFrameCallback`. This
  way state changes will be notified, and the `cooldown` will be initiated even if the function
  being called in the `onPressed` callback is asynchronous and is being awaited.
- Added the possibility to assign a `globalCustomLoader`, without the need to pass it on every
  instance creation. It can be overridden by passing a `customLoader` when the SingleTap instance is
  created and it can still be controlled whether or not to show it, by setting the `isLoaderEnabled`
  parameter to `false`.

## 0.0.10

- Cooldown fix

## 0.0.9

- Cooldown fix

## 0.0.8

- BugFix on Timeout handling
- Custom Loader widget

## 0.0.7

- BugFix on Timeout handling

## 0.0.6

- Optional `timeOut` parameter added

## 0.0.5

- Removed Unnecessary cast

## 0.0.4

- Readme update

## 0.0.3

- Clean up

## 0.0.2

- Project Settings update

## 0.0.1

- Migrated from package `single_tapable_buttons`
