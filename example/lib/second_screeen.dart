import 'dart:async';

import 'package:flutter/material.dart';
import 'package:single_tap/single_tap.dart';

class SecondScreen extends StatefulWidget {
  const SecondScreen({Key? key}) : super(key: key);

  @override
  State<SecondScreen> createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(body: Center(child: SingleTap(
      buttonType: ButtonType.elevatedButton,
      width: 300,
      // timeOut: const Duration(milliseconds: 3000),
      customLoader: const Text('A'),
      child: const Text('Tap me to go back!'),
      onPressed: (StreamSink<bool> canBePressed) async {
        // Do whatever has to be done

        // if you want to make tappable again
        // use this
        // canBePressed.add(true);
        Navigator.pop(context);

        print('Tapped me now...for custom loader');
      },
    ),),);
  }
}
