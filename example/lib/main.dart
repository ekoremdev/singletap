import 'dart:async';

import 'package:example/second_screeen.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:single_tap/single_tap.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Single Tappable Button Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Example Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({
    super.key,
    required this.title,
  });

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const Text('Loader enabled Examples'),
          Center(
            child: SingleTap(
              buttonType: ButtonType.elevatedButton,
              width: 400,
              customLoader:
                  const Text('One moment, navigating to second screen...'),
              child: const Text('Tap me now...for custom loader'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                Future.delayed(const Duration(milliseconds: 1750), () {
                  try {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SecondScreen(),
                      ),
                    );
                  } finally {
                    canBePressed.add(true);
                  }
                });

                if (kDebugMode) {
                  print('Tapped me now...for custom loader');
                }
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.textButton,
              width: 400,
              child: const Text('Tap me now...'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.iconButton,
              width: 40,
              icon: const Icon(Icons.ac_unit),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.listItem,
              width: 40,
              child: const Icon(Icons.ac_unit),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.outlinedButton,
              width: 400,
              backgroundColor: Colors.white,
              borderColor: Colors.black,
              child: const Text('ButtonType.outlinedButton'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const Divider(
            height: 1,
          ),
          const Text('Loader disabled Examples'),
          Center(
            child: SingleTap(
              buttonType: ButtonType.elevatedButton,
              width: 400,
              isLoaderEnabled: false,
              child: const Text('Tap me now...'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.textButton,
              width: 400,
              isLoaderEnabled: false,
              child: const Text('Tap me now...'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.iconButton,
              width: 40,
              isLoaderEnabled: false,
              icon: const Icon(Icons.ac_unit),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.listItem,
              width: 40,
              isLoaderEnabled: false,
              child: const Icon(Icons.ac_unit),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
          const SizedBox(height: 20),
          Center(
            child: SingleTap(
              buttonType: ButtonType.outlinedButton,
              width: 400,
              backgroundColor: Colors.white,
              borderColor: Colors.black,
              isLoaderEnabled: false,
              child: const Text('ButtonType.outlinedButton'),
              onPressed: (StreamSink<bool> canBePressed) async {
                // Do whatever has to be done

                // if you want to make tappable again
                // use this
                // canBePressed.add(true);
              },
            ),
          ),
        ],
      ),
    );
  }
}
